/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.qs = qs;
exports.$on = $on;
exports.$propagate = $propagate;
exports.isInViewport = isInViewport;
var movieDbPath = exports.movieDbPath = "https://api.themoviedb.org/3";
var movieImageBasePath = exports.movieImageBasePath = "http://image.tmdb.org/t/p/w185/";
var apiKey = exports.apiKey = "d424b71bd56ee69ccd96fcaf5d9959d8";

function qs(selector, scope) {
	return (scope || document).querySelector(selector);
}

function $on(target, type, callback, capture) {
	target.addEventListener(type, callback, !!capture);
}

function $propagate(target, selector, type, handler) {
	var dispatchEvent = function dispatchEvent(event) {
		var targetElement = event.target;
		var potentialElements = target.querySelectorAll(selector);
		var i = potentialElements.length;

		while (i--) {
			if (potentialElements[i] === targetElement || potentialElements[i] === targetElement.closest(selector)) {
				handler.call(targetElement, event);
				break;
			}
		}
	};

	$on(target, type, dispatchEvent, true);
}

function isInViewport(element) {
	var elementBounds = element.getBoundingClientRect();
	var html = document.documentElement;
	return elementBounds.top >= 0 && elementBounds.left >= 0 && elementBounds.bottom <= (window.innerHeight || html.clientHeight) && elementBounds.right <= (window.innerWidth || html.clientWidth);
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _movieService = __webpack_require__(3);

var _movieService2 = _interopRequireDefault(_movieService);

var _view = __webpack_require__(5);

var _view2 = _interopRequireDefault(_view);

var _helpers = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Controller = function () {
	function Controller() {
		var _this = this;

		_classCallCheck(this, Controller);

		this.state = {};
		this.movies = new _movieService2.default();
		this.state.moviePages = 1;
		this.state.searchModePages = 1;
		this.state.searchMode = false;

		//Get first page of now_playing movies 

		this.getGenres().then(function () {
			_this.getInTheaters();
		}).catch(function (error) {
			return console.error(new Error(error));
		});

		//set listener for loader
		this.infiniteScroll();
		//click listener for movies
		this.setupViewMore();
	}

	_createClass(Controller, [{
		key: "_enterSearchMode",
		value: function _enterSearchMode() {
			this.state.searchMode = true;
			this.state.searchModePages = 1;
			this.view.enterSearchMode();
		}
	}, {
		key: "_exitSearchMode",
		value: function _exitSearchMode() {
			this.state.searchMode = false;
			this.view.exitSearchMode();
			this.state.searchModePages = 1;
			this.state.searchQuery = "";
		}
	}, {
		key: "_enterLoading",
		value: function _enterLoading() {
			this.loading = true;
			if (this.view) {
				this.view.enterLoading();
			}
		}
	}, {
		key: "_exitLoading",
		value: function _exitLoading() {
			this.loading = false;
			this.view.exitLoading();
		}
	}, {
		key: "_getMovieElement",
		value: function _getMovieElement(element) {
			var movie = {};
			var targetElement = void 0;
			if (element.className.indexOf(".movies-item") != -1) {
				targetElement = element;
			} else {
				targetElement = element.closest(".movies-item");
			}
			movie.element = targetElement;
			movie.id = targetElement.getAttribute("data-id");
			return movie;
		}
	}, {
		key: "setupViewMore",
		value: function setupViewMore() {
			var _this2 = this;

			(0, _helpers.$propagate)((0, _helpers.qs)("section.container"), ".movies-item", "click", function (event) {
				var movie = _this2._getMovieElement(event.target);
				if (movie.element.className.indexOf("active") !== -1) {
					_this2.view.hideMovieInfo(movie.element);
				} else {
					//check if element has previously loaded
					if (movie.element.className.indexOf("loading") !== -1) {
						return;
					}
					if ((0, _helpers.qs)(".extra-info", movie.element).innerHTML == "") {
						_this2.getMovieInfo(movie);
					} else {
						_this2.view.showMovieInfo(movie.element);
					}
				}
			});
		}
	}, {
		key: "setupSearch",
		value: function setupSearch() {
			var _this3 = this;

			var searchIntent = void 0;
			var _self = this;
			var $search = (0, _helpers.qs)("#search");
			(0, _helpers.$on)($search, "keyup", function () {
				clearTimeout(searchIntent);
				searchIntent = setTimeout(function () {
					_this3._enterSearchMode();
					var query = $search.value.trim();
					if (query !== "") {
						_self.state.searchQuery = query;
						_self.searchMovies(1, query);
					} else {
						_this3._exitSearchMode();
					}
				}, 500);
			});

			(0, _helpers.$on)((0, _helpers.qs)("#clear-search"), "click", function () {
				(0, _helpers.qs)("#search").value = "";
				_this3._exitSearchMode();
			});
		}
	}, {
		key: "getGenres",
		value: function getGenres() {
			var _self = this;
			return this.movies.getGenreList().then(function (response) {
				_self.setupSearch();
				_self.genres = new Map();
				response.genres.forEach(function (genre) {
					_self.genres.set(genre.id, genre.name);
				});
				_self.view = new _view2.default(_self.genres);
			}).catch(function (response) {
				console.error(new Error(response));
			});
		}
	}, {
		key: "searchMovies",
		value: function searchMovies(page) {
			var _this4 = this;

			var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.state.searchQuery;

			if (this.loading) {
				return;
			}
			if (this.totalPagesSearch < this.state.searchModePages) {
				this.view.maxSearchResultsShown();
				return;
			}
			this._enterLoading();
			this.movies.searchMovies(page, query).then(function (response) {
				_this4.totalPagesSearch = _this4.totalPagesSearch || response.total_pages;
				_this4.view.showMovies(response.results, true);
				_this4.state.searchModePages++;
				_this4._exitLoading();
			}).catch(function (response) {
				console.error(response.status_message || "Error loading movies");
				_this4._exitLoading();
			});
		}
	}, {
		key: "getMovieInfo",
		value: function getMovieInfo(_ref) {
			var _this5 = this;

			var element = _ref.element,
			    id = _ref.id;


			this.view.enterMovieLoading(element);
			this.movies.getMovieInfo(id).then(function (values) {

				_this5.view.addMovieInfo(element, values, _this5.view.showMovieInfo);
				_this5.view.exitMovieLoading(element);
			}).catch(function (response) {
				console.log(response);
				_this5.view.exitMovieLoading(element);
			});
		}
	}, {
		key: "saveNowInTheaters",
		value: function saveNowInTheaters(results) {
			var data = {
				date: new Date().valueOf(),
				results: JSON.stringify(results)
			};
			try {
				localStorage.setItem("in-theaters", JSON.stringify(data));
			} catch (ex) {}
		}

		//one day ~86.400 seconds

	}, {
		key: "loadNowInTheaters",
		value: function loadNowInTheaters() {
			var results = null;
			if (localStorage) {
				try {
					var data = JSON.parse(localStorage.getItem("in-theaters"));
					if (data !== null && data.date) {

						var now = new Date().valueOf();
						var previousDate = data.date;
						return now - previousDate < 86000000 ? JSON.parse(data.results) : null;
					} else {
						return null;
					}
				} catch (ex) {
					return null;
				}
			}

			return results;
		}
	}, {
		key: "getInTheaters",
		value: function getInTheaters() {
			var _this6 = this;

			if (this.loading) {
				return;
			}
			if (this.totalPages < this.state.moviePages) {
				this.view.maxNowPlayingResultsShown();
				return;
			}
			this._enterLoading();
			if (this.state.moviePages === 1 && localStorage) {

				var savedResults = this.loadNowInTheaters();

				if (savedResults !== null) {
					this.view.showMovies(savedResults);
					this.state.moviePages++;
					this._exitLoading();
					return;
				}
			}

			this.movies.inTheaters(this.state.moviePages).then(function (response) {
				_this6.totalPages = _this6.totalPages || response.total_pages;
				_this6.view.showMovies(response.results);
				if (_this6.state.moviePages === 1) {
					_this6.saveNowInTheaters(response.results);
				}
				_this6.state.moviePages++;
				_this6._exitLoading();
			}).catch(function (response) {
				console.error(new Error(response.status_message || "Error loading movies"));
				_this6._exitLoading();
			});
		}
	}, {
		key: "infiniteScroll",
		value: function infiniteScroll() {
			var _this7 = this;

			var $loader = (0, _helpers.qs)("#loader");

			(0, _helpers.$on)(window, "scroll", function () {
				if (!_this7.loading && (0, _helpers.isInViewport)($loader)) {
					if (_this7.state.searchMode) {
						if (_this7.state.searchModePages > 1) {
							_this7.searchMovies(_this7.state.searchModePages, _this7.state.searchQuery);
						}
					} else {
						_this7.getInTheaters();
					}
				}
			});
		}
	}]);

	return Controller;
}();

exports.default = Controller;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _controller = __webpack_require__(1);

var _controller2 = _interopRequireDefault(_controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var controller = new _controller2.default();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _helpers = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MovieService = function () {
	function MovieService() {
		_classCallCheck(this, MovieService);
	}

	_createClass(MovieService, [{
		key: "getGenreList",
		value: function getGenreList() {
			return this._callMovieDb("genre/movie/list?api_key=" + _helpers.apiKey);
		}
	}, {
		key: "_callMovieDb",
		value: function _callMovieDb(suffixUrl) {
			var results = new Promise(function (resolve, reject) {
				var apiCall = new XMLHttpRequest();

				apiCall.onreadystatechange = function () {
					if (this.readyState == 4) {
						if (this.status === 200) {
							resolve(JSON.parse(this.response));
						} else {
							reject(this.response);
						}
					}
				};
				apiCall.open("GET", _helpers.movieDbPath + "/" + suffixUrl);
				apiCall.send();
			});
			return results;
		}
	}, {
		key: "getMovieInfo",
		value: function getMovieInfo(movieId) {
			return Promise.all([this._getSimilar(movieId), this._getReviews(movieId), this._getVideos(movieId)]);
		}
	}, {
		key: "_getSimilar",
		value: function _getSimilar(movieId) {
			return this._callMovieDb("movie/" + movieId + "/similar?api_key=" + _helpers.apiKey);
		}
	}, {
		key: "_getReviews",
		value: function _getReviews(movieId) {
			return this._callMovieDb("movie/" + movieId + "/reviews?api_key=" + _helpers.apiKey);
		}
	}, {
		key: "_getVideos",
		value: function _getVideos(movieId) {
			return this._callMovieDb("movie/" + movieId + "/videos?api_key=" + _helpers.apiKey);
		}
	}, {
		key: "inTheaters",
		value: function inTheaters() {
			var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;

			return this._callMovieDb("movie/now_playing?page=" + page + "&api_key=" + _helpers.apiKey);
		}
	}, {
		key: "searchMovies",
		value: function searchMovies() {
			var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
			var query = arguments[1];

			return this._callMovieDb("search/movie?query=" + query + "&page=" + page + "&api_key=" + _helpers.apiKey);
		}
	}]);

	return MovieService;
}();

exports.default = MovieService;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _helpers = __webpack_require__(0);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Template = function () {
	function Template(genres) {
		_classCallCheck(this, Template);

		this.genres = genres;
	}

	_createClass(Template, [{
		key: "_getSimilarHtml",
		value: function _getSimilarHtml(similar) {
			var similarHtml = "<h5 class=\"movies-item__sub-title\">Similar Movies</h5>";
			var similarCount = Math.min(similar.total_results, 4);

			if (similarCount > 0) {
				var currentSimilarMovie = void 0;
				while (similarCount--) {
					currentSimilarMovie = similar.results[similarCount];
					similarHtml += "<div class=\"movies-item__similar\">\n\t\t\t\t<h6 class=\"movies-item__similar-title\">" + currentSimilarMovie.title + "</h6>\n\t\t\t\t<img alt=\"" + currentSimilarMovie.overview + "\" src=\"" + _helpers.movieImageBasePath + currentSimilarMovie.poster_path + "\">\n\t\t\t\t</div>";
				}
				similarHtml = "<div class=\"movies-item__similar-container\">" + similarHtml + "</div>";
			} else {
				similarHtml += "<p>No Similar Movies Found</p>";
			}
			return similarHtml;
		}
	}, {
		key: "_getVideoHtml",
		value: function _getVideoHtml(video) {
			var videoCount = Math.min(video.results.length, 1);
			var videoHtml = "<h5 class=\"movies-item__sub-title\">Trailer</h5>";
			var justVideoHtml = "";
			if (videoCount) {
				justVideoHtml += "<div class=\"movies-item__similar-video\">\n\t\t\t<iframe src=\"https://www.youtube.com/embed/" + video.results[0].key + "\" frameborder=\"0\"></iframe>\n\t\t\t</div>";
			} else {
				justVideoHtml += "<p>No Videos Found</p>";
			}
			videoHtml += justVideoHtml;
			return videoHtml;
		}
	}, {
		key: "_getReviewHtml",
		value: function _getReviewHtml(reviews) {
			var reviewCount = Math.min(reviews.total_results, 2);

			var reviewHtml = "";
			var justReviewsHtml = "";
			if (reviewCount > 0) {
				var currentReview = void 0;
				while (reviewCount--) {
					currentReview = reviews.results[reviewCount];
					justReviewsHtml += "<p>" + currentReview.content + " -- " + currentReview.author + "</p>";
				}
			} else {
				justReviewsHtml += "<p>No Reviews Found</p>";
			}
			reviewHtml += "\n\t\t\t\t<div class=\"movies-item__review\">\n\t\t\t\t\t\t<h5 class=\"movies-item__sub-title\">Reviews</h5>\n\t\t\t\t\t\t" + justReviewsHtml + "\n\t\t\t\t\t</div>\n\t\t\t\t";
			return reviewHtml;
		}
	}, {
		key: "getMovieInfo",
		value: function getMovieInfo(values) {
			return "" + this._getReviewHtml(values[1]) + this._getSimilarHtml(values[0]) + this._getVideoHtml(values[2]);
		}
	}, {
		key: "getHtmlItems",
		value: function getHtmlItems(items) {
			var _self = this;
			return items.reduce(function (html, item) {
				return html + ("\n\t\t<li class=\"movies-item js-movies-item\" data-id=\"" + item.id + "\">\n\t\t\t\t\t<img alt=\"" + item.overview + "\" src=\"" + _helpers.movieImageBasePath + item.poster_path + "\" />\n\t\t\t\t\t<h4 class=\"movies-item__title\">" + item.title + "</h4>\n\t\t\t\t\t<p class=\"movies-item__overview\">" + item.overview + "</p>\n\t\t\t\t\t<span class=\"movies-item__genres\">Genres: " + item.genre_ids.reduce(function (genres, genre) {
					return genres + (_self.genres.get(genre) + ", ");
				}, '') + "</span>\n\t\t\t\t\t<span class=\"movies-item__rating-section\"><span class=\"movies-item__rating\">" + item.vote_average + "</span><span class=\"movies-item__rating-votes\">/10 (" + item.vote_count + ")</span><span class=\"movies-item__rating-votes\">Release Date: " + item.release_date + "</span></span>\n\t\t\t\t\t<div class=\"extra-info\"></div>\n\t\t\t\t</li>\n\t\t");
			}, '');
		}
	}]);

	return Template;
}();

exports.default = Template;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _template = __webpack_require__(4);

var _template2 = _interopRequireDefault(_template);

var _helpers = __webpack_require__(0);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var View = function () {
	function View(genres) {
		_classCallCheck(this, View);

		this.template = new _template2.default(genres);
		this.$movieList = (0, _helpers.qs)("#movie-list");
		this.$movieListSearch = (0, _helpers.qs)("#search-list");
	}

	_createClass(View, [{
		key: 'showMovies',
		value: function showMovies(movies) {
			var searchMode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			if (searchMode) {
				this.$movieListSearch.insertAdjacentHTML('beforeend', this.template.getHtmlItems(movies));
			} else {
				this.$movieList.insertAdjacentHTML('beforeend', this.template.getHtmlItems(movies));
			}
		}
	}, {
		key: 'addMovieInfo',
		value: function addMovieInfo(element, values, callback) {
			var extraInfo = (0, _helpers.qs)(".extra-info", element);
			extraInfo.innerHTML = this.template.getMovieInfo(values);
			if (callback) {
				callback.call(element, element);
			}
		}
	}, {
		key: 'showMovieInfo',
		value: function showMovieInfo(movieElement) {
			movieElement.classList.add("active");
		}
	}, {
		key: 'hideMovieInfo',
		value: function hideMovieInfo(movieElement) {
			movieElement.classList.remove("active");
		}
	}, {
		key: 'maxSearchResultsShown',
		value: function maxSearchResultsShown() {
			(0, _helpers.qs)("#loader").classList.add("search-full");
		}
	}, {
		key: 'maxNowPlayingResultsShown',
		value: function maxNowPlayingResultsShown() {
			(0, _helpers.qs)("#loader").classList.add("now-playing-full");
		}
	}, {
		key: 'enterMovieLoading',
		value: function enterMovieLoading(element) {
			element.classList.add("loading");
		}
	}, {
		key: 'exitMovieLoading',
		value: function exitMovieLoading(element) {
			element.classList.remove("loading");
		}
	}, {
		key: 'enterLoading',
		value: function enterLoading() {
			(0, _helpers.qs)("body").classList.add("loading");
		}
	}, {
		key: 'exitLoading',
		value: function exitLoading() {
			(0, _helpers.qs)("body").classList.remove("loading");
		}
	}, {
		key: 'enterSearchMode',
		value: function enterSearchMode() {
			(0, _helpers.qs)("body").classList.add("search");
			(0, _helpers.qs)("#search-list").innerHTML = "";
		}
	}, {
		key: 'exitSearchMode',
		value: function exitSearchMode() {
			var $bodyClasses = (0, _helpers.qs)("body").classList;
			$bodyClasses.remove("search");
			$bodyClasses.remove("search-full");
			(0, _helpers.qs)("#loader").classList.remove("search-full");
			(0, _helpers.qs)("#search-list").innerHTML = "";
		}
	}]);

	return View;
}();

exports.default = View;

/***/ })
/******/ ]);
//# sourceMappingURL=app.bundle.js.map