import {apiKey, movieDbPath, movieImageBasePath} from "./helpers";

export default class Template {

	constructor(genres) {
		this.genres = genres;
	}


	_getSimilarHtml(similar) {
		let similarHtml = `<h5 class="movies-item__sub-title">Similar Movies</h5>`;
		let similarCount = Math.min(similar.total_results, 4);

		if(similarCount > 0) {
			let currentSimilarMovie;
			while(similarCount--) {
				currentSimilarMovie = similar.results[similarCount];
				similarHtml+=`<div class="movies-item__similar">
				<h6 class="movies-item__similar-title">${currentSimilarMovie.title}</h6>
				<img alt="${currentSimilarMovie.overview}" src="${movieImageBasePath}${currentSimilarMovie.poster_path}">
				</div>`;
			}
			similarHtml = `<div class="movies-item__similar-container">${similarHtml}</div>`;
		} else {
			similarHtml+= "<p>No Similar Movies Found</p>";
		}
		return similarHtml;
	}

	_getVideoHtml(video) {
		let videoCount = Math.min(video.results.length, 1);
		let videoHtml = `<h5 class="movies-item__sub-title">Trailer</h5>`;
		let justVideoHtml = "";
		if(videoCount) {
			justVideoHtml+=`<div class="movies-item__similar-video">
			<iframe src="https://www.youtube.com/embed/${video.results[0].key}" frameborder="0"></iframe>
			</div>`;
		} else {
			justVideoHtml+= `<p>No Videos Found</p>`;
		}
		videoHtml += justVideoHtml;
		return videoHtml;
	}

	_getReviewHtml(reviews) {
		let reviewCount = Math.min(reviews.total_results, 2);

		let reviewHtml = "";
		let justReviewsHtml = "";
		if(reviewCount > 0) {
			let currentReview;
					while(reviewCount--) {
						currentReview = reviews.results[reviewCount];
						justReviewsHtml+= `<p>${currentReview.content} -- ${currentReview.author}</p>`;
					}
			
		} else {
			justReviewsHtml+= `<p>No Reviews Found</p>`;
		}
		reviewHtml+= `
				<div class="movies-item__review">
						<h5 class="movies-item__sub-title">Reviews</h5>
						${justReviewsHtml}
					</div>
				`;
		return reviewHtml;
	}

	getMovieInfo(values) {
		return `${this._getReviewHtml(values[1])}${this._getSimilarHtml(values[0])}${this._getVideoHtml(values[2])}`;
	}

	getHtmlItems(items) {
		const _self = this;
		return items.reduce((html, item) => html + `
		<li class="movies-item js-movies-item" data-id="${item.id}">
					<img alt="${item.overview}" src="${movieImageBasePath}${item.poster_path}" />
					<h4 class="movies-item__title">${item.title}</h4>
					<p class="movies-item__overview">${item.overview}</p>
					<span class="movies-item__genres">Genres: ${item.genre_ids.reduce((genres, genre)=> genres + `${_self.genres.get(genre)}, `, '')}</span>
					<span class="movies-item__rating-section"><span class="movies-item__rating">${item.vote_average}</span><span class="movies-item__rating-votes">/10 (${item.vote_count})</span><span class="movies-item__rating-votes">Release Date: ${item.release_date}</span></span>
					<div class="extra-info"></div>
				</li>
		`
	, '');
	}
}