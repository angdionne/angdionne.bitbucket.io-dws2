	const webpack = require('webpack');
const nodeEnv = process.env.NODE_ENV || 'production';

module.exports = {
	devtool: 'source-map',
	entry: {
		app: './Scripts/app.js'
	},
	output: {
		filename: '_build/[name].bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				query: {
					presets: ['es2015']
				}
			}
	]
}
	,	
	plugins: [
		// ulgify js
		// new webpack.optimize.UglifyJsPlugin({
		// 	compress: { warnings: false },
		// 	output: { comments: false },
		// 	sourcemap: true
		// }),
	]
}