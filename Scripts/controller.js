import Movies from "./movieService";
import View from "./view";
import {isInViewport, qs, $on, $propagate} from "./helpers";

export default class Controller {

	constructor() {
		this.state = {};
		this.movies = new Movies();
		this.state.moviePages = 1;
		this.state.searchModePages = 1;
		this.state.searchMode = false; 

		//Get first page of now_playing movies 
		
		this.getGenres().then(() => {this.getInTheaters();}).catch(error => console.error(new Error(error)));

		//set listener for loader
		this.infiniteScroll();
		//click listener for movies
		this.setupViewMore();
	}

	_enterSearchMode() {
		this.state.searchMode = true;
		this.state.searchModePages = 1;
		this.view.enterSearchMode();
	}

	_exitSearchMode() {
		this.state.searchMode = false;
		this.view.exitSearchMode();
		this.state.searchModePages = 1;
		this.state.searchQuery = "";
	}

	_enterLoading() {
		this.loading = true;
		if(this.view) {
			this.view.enterLoading();
		}
	}

	_exitLoading() {
		this.loading = false;
		this.view.exitLoading();
	}

	_getMovieElement(element) {
		const movie = {};
		let targetElement;
		if(element.className.indexOf(".movies-item") != -1 ) {
			targetElement = element;
			
		} else {
			targetElement = element.closest(".movies-item");
		}
		movie.element = targetElement;
		movie.id = targetElement.getAttribute("data-id");
		return movie;
	}

	setupViewMore() {
		$propagate(qs("section.container"), ".movies-item", "click", event => {
			const movie = this._getMovieElement(event.target);
			if(movie.element.className.indexOf("active") !== -1) {
				this.view.hideMovieInfo(movie.element);
			} else {
				//check if element has previously loaded
				if(movie.element.className.indexOf("loading") !== -1) {
					return;
				}
				if(qs(".extra-info", movie.element).innerHTML == "") {
					this.getMovieInfo(movie);
				} else {
					this.view.showMovieInfo(movie.element);
				}
			}
		});
	}

	setupSearch() {
		let searchIntent;
		const _self = this;
		const $search = qs("#search");
		$on($search, "keyup", () => {
			clearTimeout(searchIntent);
			searchIntent = setTimeout(() => {
				this._enterSearchMode();
				const query = $search.value.trim();
				if(query !== "") {
					_self.state.searchQuery = query;
					_self.searchMovies(1, query);
				} else {
					this._exitSearchMode();
				}
			}, 500);

			
		});

		$on(qs("#clear-search"), "click", () => {
			qs("#search").value = "";
			this._exitSearchMode();
		});
	}

	getGenres() {
		const _self = this;
		return this.movies.getGenreList().then(response => {
			_self.setupSearch();
			_self.genres = new Map();
			response.genres.forEach(genre => {
				_self.genres.set(genre.id, genre.name);
			});
			_self.view = new View(_self.genres);

		})
		.catch(response => {
			console.error(new Error(response));
		});
	}

	searchMovies (page, query = this.state.searchQuery ) {
		if(this.loading) {
			return;
		}
		if(this.totalPagesSearch < this.state.searchModePages) {
			this.view.maxSearchResultsShown();
			return;
		}
		this._enterLoading();
		this.movies.searchMovies(page, query)
		.then(response => {
			this.totalPagesSearch = this.totalPagesSearch || response.total_pages;
			this.view.showMovies(response.results, true);
			this.state.searchModePages++;
			this._exitLoading();
		})
		.catch((response) => {
			console.error(response.status_message || "Error loading movies");
			this._exitLoading();
		});
	}

	getMovieInfo({element, id}) {

		this.view.enterMovieLoading(element);
		this.movies.getMovieInfo(id).then(values => {

			this.view.addMovieInfo(element, values, this.view.showMovieInfo);
			this.view.exitMovieLoading(element);

		}).catch(response => {
			console.log(response);
			this.view.exitMovieLoading(element);
		});
	}


	saveNowInTheaters(results) {
		var data = {
			date: (new Date()).valueOf(),
			results: JSON.stringify(results)
		};
		try {
			localStorage.setItem("in-theaters", JSON.stringify(data));
		} catch (ex) {

		}
	}

	//one day ~86.400 seconds
	loadNowInTheaters() {
		var results = null;
		if(localStorage) {
			try {
				var data = JSON.parse(localStorage.getItem("in-theaters"));
				if(data !== null && data.date) {
					
					var now = new Date().valueOf();
					var previousDate = data.date;					
					return now - previousDate < 86000000 ? JSON.parse(data.results) : null;
				} else {
					return null;
				}

			} catch (ex) {
				return null;
			}
		}	

		return results;
	}

	getInTheaters () {
		if(this.loading) {
			return;
		}
		if(this.totalPages < this.state.moviePages) {
			this.view.maxNowPlayingResultsShown();
			return;
		}
		this._enterLoading();
		if(this.state.moviePages === 1 && localStorage ) {

			var savedResults = this.loadNowInTheaters();


			if(savedResults !== null) {
				this.view.showMovies(savedResults);
				this.state.moviePages++;
				this._exitLoading();
				return;
			}
		}

		this.movies.inTheaters(this.state.moviePages)
		.then(response => {
			this.totalPages = this.totalPages || response.total_pages;
			this.view.showMovies(response.results);
			if(this.state.moviePages === 1) {
				this.saveNowInTheaters(response.results);
			}
			this.state.moviePages++;
			this._exitLoading();
		})
		.catch((response) => {
			console.error(new Error(response.status_message || "Error loading movies"));
			this._exitLoading();
		});
	}

	infiniteScroll() {
		const $loader = qs("#loader");

		$on(window, "scroll", () => {
			if(!this.loading && isInViewport($loader)) {
				if(this.state.searchMode) {
					if(this.state.searchModePages > 1) {
						this.searchMovies(this.state.searchModePages, this.state.searchQuery);
					}
				} else {
					this.getInTheaters();
				}
			}
		});
	}
}
