'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();

gulp.task('sass', function () {
  return gulp.src('./Content/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./_build'))
    .pipe(browserSync.reload({stream: true}));
});
 
gulp.task('sass:watch', ['browser-sync', 'sass'], function () {
  gulp.watch('./Content/**/*.scss', ['sass']);
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
        	baseDir: "./"
        }
    });
});