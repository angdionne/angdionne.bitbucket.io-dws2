
export const movieDbPath = "https://api.themoviedb.org/3";
export const movieImageBasePath = "http://image.tmdb.org/t/p/w185/";
export const apiKey = "d424b71bd56ee69ccd96fcaf5d9959d8";

export function qs(selector, scope) {
	return (scope || document).querySelector(selector);
}


export function $on(target, type, callback, capture) {
	target.addEventListener(type, callback, !!capture);
}


export function $propagate(target, selector, type, handler) {
	const dispatchEvent = event => {
		const targetElement = event.target;
		const potentialElements = target.querySelectorAll(selector);
		let i = potentialElements.length;

		while (i--) {
			if (potentialElements[i] === targetElement || potentialElements[i] === targetElement.closest(selector)) {
				handler.call(targetElement, event);
				break;
			}
		}
	};

	$on(target, type, dispatchEvent, true);
}

export function isInViewport(element) {
	const elementBounds = element.getBoundingClientRect();
	const html = document.documentElement;
	return (elementBounds.top >= 0 &&
		    elementBounds.left >= 0 &&
		    elementBounds.bottom <= (window.innerHeight || html.clientHeight) &&
		    elementBounds.right <= (window.innerWidth || html.clientWidth));
}