import Template from './template';
import {qs, $on, $propagate} from './helpers';

export default class View {

	constructor(genres) {
		this.template = new Template(genres);
		this.$movieList = qs("#movie-list");
		this.$movieListSearch = qs("#search-list");
	}

	showMovies(movies, searchMode = false) {
		if(searchMode) {
			this.$movieListSearch.insertAdjacentHTML('beforeend', this.template.getHtmlItems(movies));
		} else {
			this.$movieList.insertAdjacentHTML('beforeend', this.template.getHtmlItems(movies));
		}
	}

	addMovieInfo(element, values, callback) {
		const extraInfo = qs(".extra-info", element);
		extraInfo.innerHTML = this.template.getMovieInfo(values);
		if(callback) {
			callback.call(element, element);
		}
	}

	showMovieInfo(movieElement) {
		movieElement.classList.add("active");
	}

	hideMovieInfo(movieElement) {
		movieElement.classList.remove("active");
	}

	maxSearchResultsShown() {
		qs("#loader").classList.add("search-full");
	}

	maxNowPlayingResultsShown() {
		qs("#loader").classList.add("now-playing-full");
	}

	enterMovieLoading(element) {
		element.classList.add("loading");
	}

	exitMovieLoading(element) {
		element.classList.remove("loading");
	}

	enterLoading() {
		qs("body").classList.add("loading");
	}

	exitLoading() {
		qs("body").classList.remove("loading");
	}

	enterSearchMode() {
		qs("body").classList.add("search");
		qs("#search-list").innerHTML = "";
	}

	exitSearchMode() {
		const $bodyClasses = qs("body").classList
		$bodyClasses.remove("search");
		$bodyClasses.remove("search-full");
		qs("#loader").classList.remove("search-full");
		qs("#search-list").innerHTML = "";
	}

} 