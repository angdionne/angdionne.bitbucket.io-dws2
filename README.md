# README #


### What the project is? ###

* This project uses the API from https://www.themoviedb.org
* This is a SPA and its features are:
* The page shows the now playing movies in theaters
* User can search for movies by typing in a search box
* Infinite Scrolling: By reaching the end of the page, it automatically brings more results using ajax, if available.
* Expand a movie: It shows additional information like reviews, similar movies, trailer, etc.


### How to run it locally ###

* To run the project locally you need the *_build* folder, *Content/Images* folder and *index.html* file
https://angdionne.bitbucket.io/dws2

### Technologies Used ###

* Sass with the scss syntax
* Gulp for scss minification
* ES6 modules and vanilla js(es6)
* Webpack for the compiled js file.
* Gulp for scss minification


### Useful npm commands ###

##### npm run gulp #####

Compiles .scss files and watches for any changes in .scss files

##### npm run webpack #####

Compiles .js files and watches for any changes in .js files
